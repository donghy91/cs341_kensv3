/*
 * E_TCPAssignment.hpp
 *
 *  Created on: 2014. 11. 20.
 *      Author: 근홍
 */

#ifndef E_TCPASSIGNMENT_HPP_
#define E_TCPASSIGNMENT_HPP_


#include <E/Networking/E_Networking.hpp>
#include <E/Networking/E_Host.hpp>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <netinet/ip.h>
#include <netinet/in.h>
 
#include <E/E_TimerModule.hpp>

#include <chrono>
 
#define TCP_HEADER_SIZE 54
#define INTERNAL_BUFFER_SIZE 2048
#define DEFAULT_MSS_BIT 512
#define DEFAULT_MSS_BYTE ( DEFAULT_MSS_BIT )
#define DEFAULT_BUFFER_BYTE ( DEFAULT_MSS_BYTE * 100)
#define DEFAULT_RTT 100*1000*1000

namespace E
{
#pragma pack(push, 4)
	struct TCPHeader{
		uint16_t srcPort;
		uint16_t destPort;

		uint32_t seqNum;
		uint32_t ackNum;

		uint8_t offset_reserved_ns;
		// 8 bits flags
		uint8_t flags;		

		uint16_t windowSize;
		uint16_t checksum;
		uint16_t urgent;
	};
#pragma pack(pop)

enum class SocketState 
{ 
	CLOSED, 
	LISTEN, 
	SYN_SENT, 
	SYN_SENT_ACK_GET,
	SYN_RECEIVED, 
	FIN_WAIT_1,
	FIN_WAIT_1_RACE, 
	FIN_WAIT_2, 
	TIMED_WAIT,
	CLOSE_WAIT,
	LAST_ACK,
	ESTABLISHED
};

enum class Flags { FIN = 0, SYN = 1, ACK = 4};

struct ConnectionRequest
{
	bool isFinished;
	uint32_t seq;
	uint32_t expected_ack; 
	sockaddr_in * src;
};

class SocketDescriptor;

struct WritePacket
{
	uint32_t seq;
	uint32_t expected_ack; 
	int size;
	UUID rtt_timer;
	Time time;
	char buffer[DEFAULT_MSS_BYTE];
	SocketDescriptor* sd;
};

struct ReceivePacket
{
	uint32_t seq;
	uint32_t expected_ack;
	int size;
	char buffer[DEFAULT_MSS_BYTE];
};


class SocketDescriptor
{
public:
	int magic_number = 25252;
	int fd;
	int pid;
	int type;  
	int protocol;
	int syscallUUID; 
	uint32_t backlog;	
	SocketState state;
	sockaddr* sock_addr; 
	socklen_t sock_length;
	sockaddr* peer_addr;
	socklen_t peer_length;
	uint32_t seq;

	typedef std::list<ConnectionRequest*> RequestQueue;
	RequestQueue* backlog_queue;
	typedef std::list<SocketDescriptor *> ConnectionRequestQueue;
	ConnectionRequestQueue * connection_request_queue;

	ConnectionRequest * close_request;

	sockaddr* return_addr;
	socklen_t* return_addr_len;

	Packet* resend_packet;
	int resend_time;
	UUID timer;

	void* input_buffer;
	int input_size;

	int write_buffer_head;
	char * write_buffer;

	int read_buffer_head;
	char * read_buffer;

	typedef std::list<WritePacket*> WritePacketList;
	WritePacketList * write_packet_list;

	WritePacketList * write_resend_list;

	typedef std::list<ReceivePacket*> ReceivePacketList;
	ReceivePacketList * receive_packet_list;

	int current_window_size;
	int current_ssh;
	int current_success_count;
	int current_fail_count;

	int fail_ofs;
	int timeout_ofs;

	int current_expected_peer_seq;

	double sampleRTT;
	double estimatedRTT;
	double devRTT;
	double timeout_interval;
};
  

class TCPAssignment : public HostModule, public NetworkModule, public SystemCallInterface, private NetworkLog, private TimerModule
{
private:
	void syscall_socket(UUID syscallUUID, int pid, int type, int protocol);
	void syscall_close(UUID syscallUUID, int pid, int s);
	void syscall_read(UUID syscallUUID, int pid, int param1, void* param2, int param3);
	void syscall_write(UUID syscallUUID, int pid, int sd, void* buffer, int size);
	void syscall_connect(UUID syscallUUID, int pid, int param1, sockaddr* addr, socklen_t len);
	void syscall_listen(UUID syscallUUID, int pid, int c, int backlog);
	void syscall_accept(UUID syscallUUID, int pid, int s, sockaddr* addr, socklen_t* len);
	void syscall_bind(UUID syscallUUID, int pid, int s, sockaddr* addr, socklen_t len);
	void syscall_getsockname(UUID syscallUUID, int pid, int s, sockaddr* addr, socklen_t* len);
	void syscall_getpeername(UUID syscallUUID, int pid, int s, sockaddr* addr, socklen_t* len);

	bool get_flags(Flags type, TCPHeader * header);
	void set_flags(Flags type, int flag, TCPHeader * header);
	bool get_bit(int number, int i);
	int set_bit(int number, int i);
	int clear_bit(int num, int i);
	
	void create_tcp_header(TCPHeader * header, sockaddr_in * src_addr, sockaddr_in * dest_addr, uint32_t seq, uint32_t ack, Flags flag, int set, void * data, int data_length);
	Packet * create_tcp_return_packet(sockaddr * src_addr, sockaddr* dest_addr, TCPHeader * header);
	Packet * create_tcp_data_packet(sockaddr * src_addr, sockaddr* dest_addr, TCPHeader * header, void * data, int size);
	uint16_t get_tcp_checksum(uint32_t source_addr, uint32_t dest_addr, TCPHeader * header, void * data, int data_length);
	bool check_tcp_checksum(const TCPHeader* header, uint32_t src_ip_int, uint32_t dest_ip_int, void * data, int data_length); 

	typedef std::pair<int,int> SocketDescriptorKey;
	typedef std::map<SocketDescriptorKey, SocketDescriptor*> SocketDescriptorMap;
	typedef std::pair<SocketDescriptorKey, SocketDescriptor*> SocketDescriptorPair;
	SocketDescriptorMap* socketDescriptorMap; 

	SocketDescriptor* allocate_socketDescriptor(int pid, int newFd, int type, int protocol);
	void free_socketDescriptor(int pid, SocketDescriptor* sd); 
	bool check_duplicate_port(sockaddr_in* new_addr); 
	SocketDescriptor* get_destination_port(sockaddr_in * target_addr);
	SocketDescriptor* find_socket_descriptor(int pid, int sd);

	int get_packet_data_length(Packet * packet);

	virtual void timerCallback(void* payload) final;

	int read_from_buffer(SocketDescriptor* sd, void* inputbuffer, int size);
	void write_from_buffer(SocketDescriptor* sd);
	void write_from_buffer();

	void send_close_packet(UUID syscallUUID, SocketDescriptor* sd);

	double get_timeout_interval(SocketDescriptor * sd);
	double get_estimated_rtt(SocketDescriptor * sd);
	double get_estimated_rtt(double sampleRTT_cur, double estimatedRTT_before);
	double get_dev_rtt(SocketDescriptor * sd);
	double get_dev_rtt(double sampleRTT_cur, double estimatedRTT_cur, double devRTT_before);

public:
	TCPAssignment(Host* host);
	virtual void initialize(); 
	virtual void finalize();
	virtual ~TCPAssignment();

protected:
	virtual void systemCallback(UUID syscallUUID, int pid, const SystemCallParameter& param) final;
	virtual void packetArrived(std::string fromModule, Packet* packet) final;
}; 

class TCPAssignmentProvider
{
private:
	TCPAssignmentProvider() {}
	~TCPAssignmentProvider() {}
public:
	static HostModule* allocate(Host* host) { return new TCPAssignment(host); }
};

} 


#endif /* E_TCPASSIGNMENT_HPP_ */