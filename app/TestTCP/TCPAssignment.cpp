/*
 * E_TCPAssignment.cpp
 *
 *  Created on: 2014. 11. 20.
 *      Author: 근홍
 */

#include <E/E_Common.hpp>
#include <E/Networking/E_Host.hpp>
#include <E/Networking/E_Networking.hpp>
#include <cerrno>
#include <E/Networking/E_Packet.hpp>
#include <E/Networking/E_NetworkUtil.hpp>
#include <E/E_TimeUtil.hpp>
#include "TCPAssignment.hpp"
#include <algorithm>
	
namespace E
{
TCPAssignment::TCPAssignment(Host* host) : HostModule("TCP", host),
		NetworkModule(this->getHostModuleName(), host->getNetworkSystem()),
		SystemCallInterface(AF_INET, IPPROTO_TCP, host),
		NetworkLog(host->getNetworkSystem()),
		TimerModule(host->getSystem())
{
 
}

TCPAssignment::~TCPAssignment()
{ 

}

void TCPAssignment::initialize()
{
	socketDescriptorMap = new SocketDescriptorMap();
}

void TCPAssignment::finalize()
{
	delete(socketDescriptorMap);
	socketDescriptorMap = NULL;
}

void TCPAssignment::systemCallback(UUID syscallUUID, int pid, const SystemCallParameter& param)
{
	switch(param.syscallNumber)
	{
	case SOCKET:
		this->syscall_socket(syscallUUID, pid, param.param1_int, param.param2_int);
		break;
	case CLOSE:
		this->syscall_close(syscallUUID, pid, param.param1_int);
		break;
	case READ:
		this->syscall_read(syscallUUID, pid, param.param1_int, param.param2_ptr, param.param3_int);
		break;
	case WRITE:
		this->syscall_write(syscallUUID, pid, param.param1_int, param.param2_ptr, param.param3_int);
		break;
	case CONNECT:
		this->syscall_connect(syscallUUID, pid, param.param1_int,
				static_cast<struct sockaddr*>(param.param2_ptr), (socklen_t)param.param3_int);
		break;
	case LISTEN:
		this->syscall_listen(syscallUUID, pid, param.param1_int, param.param2_int);
		break;
	case ACCEPT:
		this->syscall_accept(syscallUUID, pid, param.param1_int,
				static_cast<struct sockaddr*>(param.param2_ptr),
				static_cast<socklen_t*>(param.param3_ptr));
		break;
	case BIND:
		this->syscall_bind(syscallUUID, pid, param.param1_int,
				static_cast<struct sockaddr *>(param.param2_ptr),
				(socklen_t) param.param3_int);
		break;
	case GETSOCKNAME:
		this->syscall_getsockname(syscallUUID, pid, param.param1_int,
				static_cast<struct sockaddr *>(param.param2_ptr),
				static_cast<socklen_t*>(param.param3_ptr));
		break;
	case GETPEERNAME:
		this->syscall_getpeername(syscallUUID, pid, param.param1_int,
				static_cast<struct sockaddr *>(param.param2_ptr),
				static_cast<socklen_t*>(param.param3_ptr));
		break;
	default:
		assert(0); 
	}
}

void TCPAssignment::packetArrived(std::string fromModule, Packet* packet)
{
	//later, we have to compare IPv6...
 	if(fromModule.compare("IPv4") == 0)
 	{
 		uint8_t src_ip[4];
		uint8_t dest_ip[4];
		uint32_t src_ip_int;
		uint32_t dest_ip_int;

		packet->readData(14+12, src_ip, 4);
		packet->readData(14+16, dest_ip, 4);
		memcpy(&src_ip_int, src_ip, 4);
		memcpy(&dest_ip_int, dest_ip, 4);

 		size_t tcp_start = 34;
 		struct TCPHeader header;

 		packet->readData(tcp_start, &header, sizeof(struct TCPHeader));
 		
 		//Is it non-damaged packet?
 		int data_packet_length = get_packet_data_length(packet);


 		if(data_packet_length > 0)
 		{
 			char tcp_data[data_packet_length];
 			packet->readData(TCP_HEADER_SIZE, &tcp_data, data_packet_length);	

 			if(!check_tcp_checksum(&header, src_ip_int, dest_ip_int, (void *)&tcp_data, data_packet_length))
	 		{
	 			printf("Packet length : %d\n", data_packet_length);
	 			printf("Checksum Failed!\n");
	 			this->freePacket(packet);
	 			return;
	 		}	
 		}
 		else
 		{
 			if(!check_tcp_checksum(&header, src_ip_int, dest_ip_int, NULL, 0))
	 		{
	 			printf("Checksum Failed!\n");
	 			this->freePacket(packet);
	 			return;
	 		}	
 		}

 		//Find appropiate Socket Descriptor.
 		SocketDescriptor* dest_sd = NULL;
 		for(auto iter = socketDescriptorMap->begin(); iter != socketDescriptorMap->end(); iter++)
		{
			SocketDescriptor* iter_sd = iter->second;
			sockaddr_in* addr_in = (sockaddr_in*)iter_sd->sock_addr; 

			//printf("ip %d %d\n",addr_in->sin_addr.s_addr, dest_ip_int);
			//printf("port %d %d \n",addr_in->sin_port,  header.destPort);
			if(addr_in != NULL &&
				((addr_in->sin_addr.s_addr == INADDR_ANY 
				|| addr_in->sin_addr.s_addr == dest_ip_int)
				&& addr_in->sin_port == header.destPort))
			{
				sockaddr_in* peer_in = (sockaddr_in*)iter_sd->peer_addr; 
				if(peer_in == NULL)
				{
					dest_sd = iter_sd;
					continue;
				}

 				//printf("port %d %d \n",addr_in->sin_port,  header.destPort);
				if(peer_in != NULL && 
					((peer_in->sin_addr.s_addr == src_ip_int)
					&& peer_in->sin_port == header.srcPort))
				{
					dest_sd = iter_sd;
					break;
				}
			}
		}

		if(dest_sd == NULL)
		{
			printf("I can't find it....\n");
			this->freePacket(packet);
			return;
		}
 
		//printf("Packet to %d %d\n", dest_sd->pid, dest_sd->fd);
 		if(get_flags(Flags::SYN, &header))
 		{
 			if(get_flags(Flags::ACK, &header))	
 			{
 				//syn + ack packet arrived
 				//check your seqNum is appropriate
 				if(dest_sd->state == SocketState::SYN_SENT)
 				{
	 				printf("syn ack!\n");

					dest_sd->state = SocketState::ESTABLISHED;
					dest_sd->current_expected_peer_seq = ntohl(header.seqNum) + 1;

	 				TCPHeader ackHeader = {0};
	 				memcpy(&ackHeader, &header, sizeof(TCPHeader));

	 				ackHeader.srcPort = header.destPort;
	 				ackHeader.destPort = header.srcPort;
	 				ackHeader.seqNum = htonl(dest_sd->seq);
	 				ackHeader.ackNum = htonl(ntohl(header.seqNum) + 1);
	 				dest_sd->current_expected_peer_seq = ntohl(header.seqNum) + 1;

	 				printf("SYNACK to SYN_SENT\n");
 					printf("resultack : %u\n", ntohl(header.ackNum));
 					printf("resultseq : %u\n", ntohl(header.seqNum));

 					set_flags(Flags::SYN, 0, &ackHeader);
	 				set_flags(Flags::ACK, 1, &ackHeader);
					ackHeader.checksum = get_tcp_checksum(dest_ip_int, src_ip_int, &ackHeader, NULL, 0);
					Packet * ack_packet = this->allocatePacket(packet->getSize());

					ack_packet->writeData(14+12, dest_ip, 4); 
					ack_packet->writeData(14+16, src_ip, 4);
					ack_packet->writeData(tcp_start, &ackHeader, sizeof(TCPHeader));

					this->sendPacket("IPv4", ack_packet);
					this->freePacket(packet);

					returnSystemCall(dest_sd->syscallUUID, 0);
					dest_sd->syscallUUID = -1;
					return;
 				}
 			}
 			else
 			{
 				//SYN only packet Arrived
 				if(dest_sd->state == SocketState::CLOSED)
 				{
 					printf("connect before listen!!\n");
 					this->freePacket(packet);
 					return;
 				}
 				else
 				{
 					if(dest_sd->state == SocketState::SYN_SENT_ACK_GET || 
 						 dest_sd->state == SocketState::SYN_SENT)
 					{
 						printf("WOW! ALONE SYN!!=== %d\n", ntohl(header.ackNum));

						dest_sd->state = SocketState::ESTABLISHED;
						dest_sd->current_expected_peer_seq = ntohl(header.seqNum) + 1;

 						TCPHeader ackHeader = {0};
		 				memcpy(&ackHeader, &header, sizeof(TCPHeader));

		 				ackHeader.srcPort = header.destPort;
		 				ackHeader.destPort = header.srcPort;
		 				ackHeader.seqNum = htonl(dest_sd->seq);
		 				ackHeader.ackNum = htonl(ntohl(header.seqNum) + 1);
		 				set_flags(Flags::SYN, 0, &ackHeader); 
		 				set_flags(Flags::ACK, 1, &ackHeader); 
						ackHeader.checksum = get_tcp_checksum(dest_ip_int, src_ip_int, &ackHeader, NULL, 0);

						Packet * ack_packet = this->allocatePacket(packet->getSize());

						ack_packet->writeData(14+12, dest_ip, 4); 
						ack_packet->writeData(14+16, src_ip, 4);
						ack_packet->writeData(tcp_start, &ackHeader, sizeof(TCPHeader));

						this->sendPacket("IPv4", ack_packet);
						this->freePacket(packet);

						returnSystemCall(dest_sd->syscallUUID, 0);
						dest_sd->syscallUUID = -1;
						return;
 					}

 					if(dest_sd->state == SocketState::LISTEN)
 					{
 						if(dest_sd->backlog_queue->size() < dest_sd->backlog)
	 					{
 							printf("dp sp %d %d\n",header.destPort, header.srcPort);
 							printf("pid fd? %d %d\n",dest_sd->pid, dest_sd->fd);
	 						TCPHeader ackHeader = {0};
	 						memcpy(&ackHeader, &header, sizeof(TCPHeader));

	 						ackHeader.srcPort = header.destPort;
	 						ackHeader.destPort = header.srcPort;
	 						ackHeader.seqNum =	htonl(dest_sd->seq);
	 						ackHeader.ackNum = htonl(ntohl(header.seqNum) + 1);

			 				set_flags(Flags::SYN, 1, &ackHeader);
			 				set_flags(Flags::ACK, 1, &ackHeader);
							ackHeader.checksum = get_tcp_checksum(dest_ip_int, src_ip_int, &ackHeader, NULL, 0);
							Packet * syn_ack_packet = this->allocatePacket(packet->getSize());

							syn_ack_packet->writeData(14+12, dest_ip, 4); 
							syn_ack_packet->writeData(14+16, src_ip, 4);
							syn_ack_packet->writeData(tcp_start, &ackHeader, sizeof(TCPHeader));

							this->sendPacket("IPv4", syn_ack_packet);
							this->freePacket(packet);

							//Insert to Backlog
							ConnectionRequest * request = (ConnectionRequest *)malloc(sizeof(ConnectionRequest));
			 				request->expected_ack = htonl(ntohl(header.seqNum));
			 				request->src = (sockaddr_in*)malloc(sizeof(sockaddr_in));
			 				request->src->sin_family = AF_INET;
	 						request->src->sin_port = header.srcPort;
	 						request->src->sin_addr.s_addr = src_ip_int;

			 				dest_sd->backlog_queue->push_back(request);
							return;
	 					}
						else
						{
							printf("sorry! full backlog!! %d\n", dest_sd->backlog);
							this->freePacket(packet);
							return;
						}
 					}
 					else
 					{
 						//printf("dp sp %d %d\n",header.destPort, header.srcPort);
 						//printf("state %d\n",static_cast<int>(dest_sd->state));
 					}

 				}

 			}
 		}
 		else if (get_flags(Flags::ACK, &header))
 		{
 			//printf("===============================ACK get===========================\n");
 			//printf("dest window size %d\n", header.windowSize);

 			if(dest_sd->state == SocketState::SYN_SENT)
 			{
 				printf("WOW! ALONE ACK!!=== %d\n", ntohl(header.ackNum));
 				if(ntohl(header.ackNum) == dest_sd->seq)
 				{
 					dest_sd->state = SocketState::SYN_SENT_ACK_GET;
 				}
				this->freePacket(packet);
				return;
 			}

 			if(dest_sd->state == SocketState::LISTEN)
 			{
 				auto cl = dest_sd->backlog_queue;
 				printf("HEY %d connection Waiting ACK!\n", (int)cl->size());
 				ConnectionRequest * rq = NULL;
 				for(auto iter = cl->begin(); iter != cl->end(); ++iter)
 				{
 					rq = (ConnectionRequest *)*iter;
 					sockaddr_in* addr_in = rq->src;
 					if(addr_in->sin_port == header.srcPort
 						&& addr_in->sin_addr.s_addr == src_ip_int)
 					{
 						printf("WOW I'VE WAITED FOR YOU!\n");
 						cl->erase(iter);
 						break;
 					}

 				}

 				if(rq != NULL)
 				{
 					SocketDescriptor* new_sd = allocate_socketDescriptor(dest_sd->pid, -1, dest_sd->type, dest_sd->protocol);

 					new_sd->sock_addr = (sockaddr*)malloc(sizeof(sockaddr));
 					memcpy(new_sd->sock_addr, dest_sd->sock_addr, sizeof(sockaddr));
 					new_sd->sock_length = sizeof(sockaddr);


 					sockaddr_in* ta = (sockaddr_in*)new_sd->sock_addr;
 					if(ta->sin_addr.s_addr == 0)
 					{
 						ta->sin_addr.s_addr = dest_ip_int;
 					}
 					
 					new_sd->peer_addr = (sockaddr*)malloc(sizeof(sockaddr));
 					memcpy(new_sd->peer_addr, rq->src, sizeof(sockaddr));
 					new_sd->peer_length = sizeof(sockaddr);

 					new_sd->state = SocketState::ESTABLISHED;
 					new_sd->seq = 1;
 					if(dest_sd->return_addr == NULL)
 					{
			 						
			 			printf("====================ACK BEFORE ACCEPT ===========================\n");
			 			printf("src_port : %d\n", header.srcPort);
			 			printf("dest_port : %d\n", header.destPort);
			 			printf("flags : %d\n", header.flags);
			 			printf("=================================================================\n");
 						dest_sd->connection_request_queue->push_back(new_sd);
 					}
 					else
 					{
 						//dest_sd->connection_request_queue->push(new_sd);
 						
						int newFd = createFileDescriptor(dest_sd->pid);
						if(newFd < 0 )
						{
							returnSystemCall(dest_sd->syscallUUID, -1);
							dest_sd->syscallUUID = -1;
							dest_sd->return_addr = NULL;
							dest_sd->return_addr_len = NULL;
							return;
						}
						new_sd->fd = newFd;
						socketDescriptorMap->insert(SocketDescriptorPair(SocketDescriptorKey(new_sd->pid, new_sd->fd), new_sd));
						memcpy(dest_sd->return_addr, new_sd->peer_addr, sizeof(sockaddr));
						*(dest_sd->return_addr_len) = sizeof(sockaddr);

						new_sd->current_expected_peer_seq = ntohl(header.seqNum);

						dest_sd->return_addr = NULL;
						dest_sd->return_addr_len = NULL;
						returnSystemCall(dest_sd->syscallUUID, new_sd->fd);
						dest_sd->syscallUUID = -1;
						printf("FINISH!!!! %d %d\n",new_sd->pid , new_sd->fd);
						
 					}
 				}
 				this->freePacket(packet);
 				return;
 			}
 			else if (dest_sd->state == SocketState::FIN_WAIT_1_RACE)
 			{
 				printf("FIN_WAIT_1_RACE to TIMED_WAIT\n");
 				if(dest_sd->close_request->expected_ack == header.ackNum)
	 			{
	 				dest_sd->state = SocketState::TIMED_WAIT;

					if(dest_sd->timer > 0)
 					{
 						printf("there is a timer!\n");
 						dest_sd->resend_time = 0;

 						if(dest_sd->resend_packet != NULL)
 						{
 							this->freePacket(dest_sd->resend_packet);
 							dest_sd->resend_packet = NULL;
 						}

 						this->cancelTimer(dest_sd->timer);
 						dest_sd->timer = -1;
 					}

 					dest_sd->timer = this->addTimer(dest_sd, TimeUtil::makeTime(4, TimeUtil::TimeUnit::MINUTE));	

 					this->freePacket(packet);	

 					returnSystemCall(dest_sd->syscallUUID, 0);	
 					dest_sd->syscallUUID = -1;
					return;	
	 			}
 			}
 			else if (dest_sd->state == SocketState::LAST_ACK)
 			{
 				//finish it.
 				printf("ACK to LAST_ACK\n");
 				if(dest_sd->close_request->expected_ack == header.ackNum)
 				{
 					UUID uuid = dest_sd->syscallUUID;

					socketDescriptorMap->erase(SocketDescriptorKey(dest_sd->pid, dest_sd->fd));
	 				free_socketDescriptor(dest_sd->pid, dest_sd);

					this->freePacket(packet);
					returnSystemCall(uuid, 0);			
					printf("Finished server!\n");
					return;	
 				}
 			}
 			else if (dest_sd->state == SocketState::ESTABLISHED || dest_sd->state == SocketState::FIN_WAIT_1)
 			{
 				//printf("ACK to ESTABLISHED\n");
 				//data transfer
 				size_t data_size = ( packet->getSize() - (34 + sizeof(TCPHeader)) );

 				if(data_size == 0)
 				{
 					if(dest_sd->state == SocketState::FIN_WAIT_1)
 					{
		 				if(dest_sd->close_request->expected_ack == ntohl(header.ackNum))
		 				{
		 					dest_sd->state = SocketState::FIN_WAIT_2;	
		 				}

		 				this->freePacket(packet);
		 				return;
 					}
 					//ACK to Send Data

 					if(dest_sd->write_packet_list->size() == 0)
 					{
 						printf("?????????????????\n");
		 				this->freePacket(packet);
		 				return;
 					}

 					printf("== %d %d == %d %d\n",dest_sd->write_packet_list->front()->expected_ack, ntohl(header.ackNum), 
 						dest_sd->current_window_size, dest_sd->write_packet_list->size());
 					WritePacket * front_packet = dest_sd->write_packet_list->front();
 					if(front_packet->expected_ack == ntohl(header.ackNum))
 					{
 						//printf("Success %d\n", front_packet->expected_ack);
 						dest_sd->current_fail_count = 0;
 						if(dest_sd->current_window_size < dest_sd->current_ssh )
 						{
	 						dest_sd->current_window_size++;
 						}
 						else
 						{
 							dest_sd->current_success_count++;
	 						if(dest_sd->current_success_count >= dest_sd->current_window_size)
	 						{
	 							dest_sd->current_window_size+=1;
	 							dest_sd->current_success_count = 0;
	 						}
 						}
 						
 						//printf("timer cancle %d\n", front_packet->rtt_timer);
 						this->cancelTimer(front_packet->rtt_timer);
 						auto end_time = this->getHost()->getSystem()->getCurrentTime();

 						Time test_time = end_time - front_packet->time;
 						Size rtt = TimeUtil::getTime(test_time, TimeUtil::TimeUnit::USEC);

 						//printf("SampleRTT : %d\n", rtt);
 						dest_sd->sampleRTT = rtt / 1000.0f;

 						get_timeout_interval(dest_sd);
 						//printf("timeout interval : %f\n", dest_sd->timeout_interval);

 						dest_sd->seq = front_packet->expected_ack;
 						dest_sd->write_packet_list->pop_front();

 					}
 					else if(dest_sd->write_packet_list->front()->expected_ack < ntohl(header.ackNum))
 					{
 						//printf("Large ACK\n");
 						dest_sd->current_fail_count = 0;
 						while(dest_sd->write_packet_list->size() > 0)
 						{
 							if(dest_sd->write_packet_list->front()->expected_ack <= ntohl(header.ackNum))
 							{
 								dest_sd->seq = dest_sd->write_packet_list->front()->expected_ack;
 								this->cancelTimer(dest_sd->write_packet_list->front()->rtt_timer);
 								dest_sd->write_packet_list->pop_front();
 							}
 							else
 								break;
 						}
 					}
 					else
 					{
 						dest_sd->current_success_count = 0;
 						dest_sd->timeout_ofs++;
 						if(dest_sd->fail_ofs > 0)
 						{
 							//printf("SO SAD...?\n");
 							dest_sd->fail_ofs--;
 							dest_sd->current_window_size++;
 						}
 						else
 						{
 							printf("whatthe...?\n");
 							dest_sd->current_fail_count++;
	 						if(dest_sd->current_fail_count > 2)
	 						{
	 							dest_sd->current_fail_count = 0;

	 							dest_sd->current_window_size /= 2;
	 							if(dest_sd->current_window_size == 0)
	 								dest_sd->current_window_size++;

	 							dest_sd->fail_ofs = dest_sd->write_packet_list->size() - 1;

	 							WritePacket* failed_packet = dest_sd->write_packet_list->front(); 

 								this->cancelTimer(failed_packet->rtt_timer);
	 							//if(failed_packet->seqNum != )							
	 							TCPHeader header = {0};
								sockaddr_in* src_addr = (sockaddr_in*)dest_sd->sock_addr;
								sockaddr_in* dest_addr = (sockaddr_in*)dest_sd->peer_addr; 

								create_tcp_header(&header, src_addr, dest_addr, 
									htonl(failed_packet->seq), htonl(dest_sd->current_expected_peer_seq), Flags::ACK, 1, failed_packet->buffer, failed_packet->size);
								Packet * packet = create_tcp_data_packet(dest_sd->sock_addr, dest_sd->peer_addr, &header, failed_packet->buffer, failed_packet->size);

								this->sendPacket("IPv4", packet);
								printf("RETRNASMIT... waitiong %d\n",failed_packet->expected_ack );
	 						}
 						}
	 					//printf("timeout ofs %d\n",  dest_sd->timeout_ofs);
 						
 						//CONGESTION CONTROL
 					}

					write_from_buffer(dest_sd);
 					this->freePacket(packet);
 					return;

 				}
 				else
 				{
 					//Received Data
 					if(dest_sd->read_buffer_head + data_size + dest_sd->receive_packet_list->size()*DEFAULT_MSS_BYTE > DEFAULT_BUFFER_BYTE)
 					{
 						//drop packet
 						this->freePacket(packet);
 						return;
 					}

 					//printf("ha : %d %d\n", (int)ntohl(header.seqNum), dest_sd->current_expected_peer_seq );
 					if(ntohl(header.seqNum) == dest_sd->current_expected_peer_seq)
					{
						packet->readData(34 + sizeof(TCPHeader), dest_sd->read_buffer + dest_sd->read_buffer_head, data_size);
						dest_sd->read_buffer_head += data_size;
						dest_sd->current_expected_peer_seq += data_size;

						while(dest_sd->receive_packet_list->size() > 0)
						{
							ReceivePacket* p = dest_sd->receive_packet_list->front();
							if(dest_sd->current_expected_peer_seq != p->seq )
								break;
							memcpy(dest_sd->read_buffer + dest_sd->read_buffer_head, p->buffer, p->size);
							dest_sd->current_expected_peer_seq = p->expected_ack;
							dest_sd->receive_packet_list->pop_front();
						}
					}
					else
					{
						//wrong packet
						if(ntohl(header.seqNum) > dest_sd->current_expected_peer_seq)
						{
							//duplicate data ignore;
 							this->freePacket(packet);
							return;
						}
						
						ReceivePacket * rp = new ReceivePacket();
						rp->seq = ntohl(header.seqNum);
						rp->expected_ack = rp->seq + data_size;

						packet->readData(34 + sizeof(TCPHeader), rp->buffer, data_size);
						dest_sd->receive_packet_list->push_back(rp);
						
					}

					TCPHeader ack_header = {0};
 					create_tcp_header(&ack_header, (sockaddr_in *)dest_sd->sock_addr, (sockaddr_in *)dest_sd->peer_addr, 
 									  htonl(dest_sd->seq), htonl(dest_sd->current_expected_peer_seq), Flags::ACK, 1, NULL, 0);
 					Packet * ack_packet = create_tcp_return_packet(dest_sd->sock_addr, dest_sd->peer_addr, &ack_header);
 					this->sendPacket("IPv4", ack_packet);

 					if(dest_sd->syscallUUID >= 0)
 					{
 						if(dest_sd->read_buffer_head > 0)
 						{
 							//if still read buffer is empty, do not return.

 							int read_size = read_from_buffer(dest_sd, dest_sd->input_buffer, dest_sd->input_size);
 							dest_sd->input_buffer = NULL;
 							dest_sd->input_size = 0;
 							returnSystemCall(dest_sd->syscallUUID, read_size);
 							dest_sd->syscallUUID = -1;	
 						}
 					}

 					this->freePacket(packet);
 					return;
 				}
 			}
 			else
 			{
 				int temp = static_cast<int>(dest_sd->state);
 				printf("UNKNWON state to ACK : %d\n", temp);
 				printf("Flags : %d\n", header.flags);
 			}
 		}
 		else if (get_flags(Flags::FIN, &header))
 		{
 			//close connection
 			//printf("~FIN get!\n");
 			/*printf("src_port : %d\n", header.srcPort);
 			printf("dest_port : %d\n", header.destPort);
 			printf("flags : %d\n", header.flags);
 			printf("=================================================================\n");*/

 			if(dest_sd->state == SocketState::ESTABLISHED )//|| dest_sd->state == SocketState::LISTEN)
 			{ 
 				if(dest_sd->syscallUUID >= 0)
 				{
 					returnSystemCall(dest_sd->syscallUUID, 0);
 					dest_sd->syscallUUID = -1;
 				}
 			}
 			else if (dest_sd->state == SocketState::LISTEN)
 			{
 				printf("dest_sd->seq :%d\n", dest_sd->seq);
 				auto waiting_q = dest_sd->connection_request_queue;
 				for(auto iter = waiting_q->begin(); iter != waiting_q->end(); iter++)
				{
					SocketDescriptor* waiting_sd = *iter;
					sockaddr_in* addr_in = (sockaddr_in*)waiting_sd->peer_addr; 

					if(addr_in != NULL &&
						((addr_in->sin_addr.s_addr == INADDR_ANY 
						|| addr_in->sin_addr.s_addr == src_ip_int)
						&& addr_in->sin_port == header.srcPort))
					{
						waiting_sd->state = SocketState::CLOSE_WAIT;

						TCPHeader ack_header = {0};
 						sockaddr_in * src_addr = (sockaddr_in *)malloc(sizeof(sockaddr_in));
 						src_addr->sin_family = AF_INET;
 						src_addr->sin_port = header.srcPort;
 						src_addr->sin_addr.s_addr = src_ip_int;

 						create_tcp_header(&ack_header, (sockaddr_in *)waiting_sd->sock_addr, src_addr, htonl(waiting_sd->seq), htonl(ntohl(header.seqNum) + 1), Flags::ACK, 1, NULL, 0);
 						set_flags(Flags::FIN, 0, &ack_header);

 						Packet * ack_packet = create_tcp_return_packet(waiting_sd->sock_addr, (sockaddr *)src_addr, &ack_header);
 						this->sendPacket("IPv4", ack_packet);
 						this->freePacket(packet); 
						return;
					}	
				}

 				printf("???????????????????????????????????SHOULD NOT HAPPEND!\n");
 			}
 			else if (dest_sd->state == SocketState::FIN_WAIT_1)
 			{
 				printf("dest_sd->seq :%d\n", dest_sd->seq);
 				printf("FIN_WAIT_1 to FIN_WAIT_1_RACE\n");
 				dest_sd->state = SocketState::FIN_WAIT_1_RACE;

 				//if we in FIN_WAIT_1_RACE
 				TCPHeader ack_header = {0};
 				create_tcp_header(&ack_header, (sockaddr_in *)dest_sd->sock_addr, (sockaddr_in *)dest_sd->peer_addr, 
 									htonl(dest_sd->seq), htonl(ntohl(header.seqNum) + 1), 
 									Flags::ACK, 1, NULL, 0);

 				Packet * ack_packet = create_tcp_return_packet(dest_sd->sock_addr, dest_sd->peer_addr, &ack_header);
 				this->sendPacket("IPv4", ack_packet);
 				this->freePacket(packet); 
 				return;
 			}
 			else if (dest_sd->state == SocketState::FIN_WAIT_2)
 			{
 				dest_sd->state = SocketState::TIMED_WAIT;

 				printf("==========================================================\n");
 				printf("FIN to FIN_WAIT_2\n");
 				printf("seq : %u\n", ntohl(header.seqNum));
 				printf("ack : %u\n", ntohl(header.ackNum));
 				printf("==========================================================\n");

 				TCPHeader ack_header = {0};
 				create_tcp_header(&ack_header, (sockaddr_in *)dest_sd->sock_addr, (sockaddr_in *)dest_sd->peer_addr, 
 									htonl(dest_sd->seq), htonl(ntohl(header.seqNum) + 1), 
 									Flags::ACK, 1, NULL, 0);
 				set_flags(Flags::FIN, 0, &ack_header);

 				Packet * ack_packet = create_tcp_return_packet(dest_sd->sock_addr, dest_sd->peer_addr, &ack_header);

 				dest_sd->timer = this->addTimer(dest_sd, TimeUtil::makeTime(4, TimeUtil::TimeUnit::MINUTE));

 				this->sendPacket("IPv4", ack_packet);

 				this->freePacket(packet);	

 				returnSystemCall(dest_sd->syscallUUID, 0);	
 				return;
 			}
 			else if (dest_sd->state == SocketState::TIMED_WAIT)
 			{
				TCPHeader ack_header = {0};
 				sockaddr_in * src_addr = (sockaddr_in *)malloc(sizeof(sockaddr_in));
 				src_addr->sin_family = AF_INET;
 				src_addr->sin_port = header.srcPort;
 				src_addr->sin_addr.s_addr = src_ip_int;

 				create_tcp_header(&ack_header, (sockaddr_in *)dest_sd->sock_addr, src_addr, 
 									htonl(dest_sd->seq), htonl(ntohl(header.seqNum) + 1), 
 									Flags::ACK, 1, NULL, 0);
 				set_flags(Flags::FIN, 0, &ack_header);

 				Packet * ack_packet = create_tcp_return_packet(dest_sd->sock_addr, (sockaddr *)src_addr, &ack_header);
 				this->sendPacket("IPv4", ack_packet);
 				this->freePacket(packet); 		 				

 				printf("TIMED_WAIT\n");
 				return;
 			}
 			else if (dest_sd->state == SocketState::CLOSE_WAIT)
 			{
 				TCPHeader ack_header = {0};
 				sockaddr_in * src_addr = (sockaddr_in *)malloc(sizeof(sockaddr_in));
 				src_addr->sin_family = AF_INET;
 				src_addr->sin_port = header.srcPort;
 				src_addr->sin_addr.s_addr = src_ip_int;

 				create_tcp_header(&ack_header, (sockaddr_in *)dest_sd->sock_addr, src_addr, 
 									htonl(dest_sd->seq), htonl(ntohl(header.seqNum) + 1), 
 									Flags::ACK, 1, NULL, 0);
 				set_flags(Flags::FIN, 0, &ack_header);

 				Packet * ack_packet = create_tcp_return_packet(dest_sd->sock_addr, (sockaddr *)src_addr, &ack_header);
 				this->sendPacket("IPv4", ack_packet);
 				this->freePacket(packet); 		

 				free(src_addr);

 				printf("Close_wait -> LAST_ACK\n");
 				return;
 			}
 			else if (dest_sd->state == SocketState::LAST_ACK)
 			{
 				//we cannot send data when we changed the state to LAST_ACK;
 				printf("LAST_ACK to CLOSED\n");	
 			}
 			else
 			{
 				//what is this state?
 				//printf("what is this state?\n");
 				int temp = static_cast<int>(dest_sd->state);
 				printf("==========================================================\n");
 				printf("FIN to ?????? %d\n", temp);
 				printf("ack : %u\n", ntohl(header.ackNum));
 				printf("seq : %u\n", ntohl(header.seqNum));
 				printf("==========================================================\n");
 				return;
 			}
 		}
 		else if(dest_sd->state == SocketState::SYN_SENT)
 		{
 				printf("===============================PACKET get===========================\n");
 		}
 		else
 		{
 			//Normal packet, maybe?
 		}

 	}
 	else if (fromModule.compare("Host"))
 	{

 	}
 	else
 	{
 		assert(0);
 	}
}

void TCPAssignment::timerCallback(void* payload)
{
		SocketDescriptor* sd = (SocketDescriptor *)payload;
		if(sd->magic_number == 25252)
		{
			if(sd->state == SocketState::TIMED_WAIT)
			{
				printf("Timer out! close!\n");
				printf("pid : %d\n", sd->pid);
				printf("fd : %d\n", sd->fd);
				socketDescriptorMap->erase(SocketDescriptorKey(sd->pid, sd->fd));
				free_socketDescriptor(sd->pid, sd);
			}
		}
		else
		{
			WritePacket * packet = (WritePacket *)payload;
			SocketDescriptor * sd = packet->sd;


			printf("TIMEOUT %d %d\n",packet->expected_ack,packet->rtt_timer);

			sd->current_success_count = 0;
			sd->current_ssh = sd->current_window_size / 2;
			if(sd->current_ssh <= 0)
				sd->current_ssh = 1;
			sd->current_window_size = 1;

			//sd->fail_ofs = sd->write_packet_list->size();

			//sd->write_packet_list->splice(sd->write_packet_list->begin(),*sd->write_resend_list);

			for(auto iter=sd->write_packet_list->begin(); iter != sd->write_packet_list->end();++iter)
			{
				WritePacket* wp = *iter;
 				this->cancelTimer(wp->rtt_timer);
			}
			sd->write_resend_list->splice(sd->write_resend_list->begin(),*sd->write_packet_list);
			write_from_buffer(sd); 

			/*
			TCPHeader header = {0};
			sockaddr_in* src_addr = (sockaddr_in*)sd->sock_addr;
			sockaddr_in* dest_addr = (sockaddr_in*)sd->peer_addr; 
			create_tcp_header(&header, src_addr, dest_addr, htonl(packet->seq), htonl(0), Flags::ACK, 1, packet->buffer, packet->size);
			Packet * resend_packet = create_tcp_data_packet(sd->sock_addr, sd->peer_addr, &header, packet->buffer, packet->size);

			this->sendPacket("IPv4", resend_packet);

			packet->rtt_timer = this->addTimer(sd, TimeUtil::makeTime(sd->timeout_interval, TimeUtil::TimeUnit::MSEC));
			packet->start = std::chrono::high_resolution_clock::now();
			*/
		}
}

void TCPAssignment::syscall_socket(UUID syscallUUID, int pid, int type, int protocol)
{
	int newFd = createFileDescriptor(pid);
	if(newFd < 0 )
	{
		returnSystemCall(syscallUUID, -1);
		return;
	}

	SocketDescriptor* newSd = allocate_socketDescriptor(pid, newFd, type, protocol);
	socketDescriptorMap->insert(SocketDescriptorPair(SocketDescriptorKey(newSd->pid, newSd->fd), newSd));
	returnSystemCall(syscallUUID,newSd->fd);
	return;
}	

void TCPAssignment::syscall_close(UUID syscallUUID, int pid, int s)
{
	//printf("SYSCALL CLOSE==============================================%d %d\n",pid, s);
	SocketDescriptor* sd = find_socket_descriptor(pid, s);
	if(sd != NULL)
	{
		//if socket descriptor was not established connection, then just free it. 

		if(sd->write_buffer_head !=0 || 
			sd->write_packet_list->size() != 0 ||
			sd->write_resend_list->size() != 0)
		{
			printf("SYSCALL BLCOK\n");
			sd->syscallUUID = syscallUUID;
			return;
		}
		send_close_packet(syscallUUID, sd);
	}
	else
	{
		returnSystemCall(syscallUUID, -1);
		return;
	}
}

void TCPAssignment::send_close_packet(UUID syscallUUID, SocketDescriptor* sd)
{

	if(sd->state != SocketState::ESTABLISHED && sd->state != SocketState::LISTEN)
	{
		if(sd->state == SocketState::CLOSE_WAIT)
		{
			//send fin packet also.
			printf("===============CLOSE CLOSE_WAIT==============\n");
			TCPHeader header = {0};

			create_tcp_header(&header, (sockaddr_in *)sd->sock_addr, (sockaddr_in *)sd->peer_addr, htonl(sd->seq), htonl(0), Flags::FIN, 1, NULL, 0);
			Packet * fin_packet = create_tcp_return_packet(sd->sock_addr, sd->peer_addr, &header );

			this->sendPacket("IPv4", fin_packet);

			sd->seq += 1;

			ConnectionRequest * close_request = (ConnectionRequest *)malloc(sizeof(ConnectionRequest));
			close_request->isFinished = false;
			close_request->seq = htonl(sd->seq);
			close_request->expected_ack = htonl(sd->seq + 1);

			sd->close_request = close_request;
			sd->syscallUUID = syscallUUID;
			sd->state = SocketState::LAST_ACK;	

			printf("UUID at close : %lu\n", (uint64_t)syscallUUID);
		}
		else
		{
			socketDescriptorMap->erase(SocketDescriptorKey(sd->pid, sd->fd));
			free_socketDescriptor(sd->pid, sd);
			
			returnSystemCall(syscallUUID,0);			
			return;	
		}
	}
	else
	{
		if (sd->state == SocketState::LISTEN)
		{
			printf("Listen?\n");
			socketDescriptorMap->erase(SocketDescriptorKey(sd->pid, sd->fd));
			free_socketDescriptor(sd->pid, sd);

			returnSystemCall(syscallUUID,0);			
			return;	
		}
		else
		{
			//if connection was established, we have to handshake with connection.
			TCPHeader header = {0};
			
			create_tcp_header(&header, (sockaddr_in *)sd->sock_addr, (sockaddr_in *)sd->peer_addr, 
								htonl(sd->seq), htonl(0), 
								Flags::FIN, 1, NULL, 0);
			Packet * fin_packet = create_tcp_return_packet(sd->sock_addr, sd->peer_addr, &header );

			sockaddr_in* temp1 = (sockaddr_in*)sd->sock_addr;
			sockaddr_in* temp2 = (sockaddr_in*)sd->peer_addr;

			printf("sock addr : %d %d\n", ntohl(temp1->sin_addr.s_addr), ntohs(temp1->sin_port));
			printf("peer addr : %d %d\n", ntohl(temp2->sin_addr.s_addr), ntohs(temp2->sin_port));

			this->sendPacket("IPv4", fin_packet);

			sd->seq += 1;

			ConnectionRequest * close_request = (ConnectionRequest *)malloc(sizeof(ConnectionRequest));
			close_request->isFinished = false;
			close_request->seq = htonl(sd->seq);
			close_request->expected_ack = htonl(sd->seq + 1);

			sd->close_request = close_request;
			sd->syscallUUID = syscallUUID;

			printf("UUID at close: %lu\n", (uint64_t)syscallUUID);

			if(sd->state == SocketState::ESTABLISHED)
			{
				sd->state = SocketState::FIN_WAIT_1;
			}

			printf("======CLOSE====== %d %d\n", sd->pid , sd->fd);
				printf("seq : %d\n", ntohl(header.seqNum));
				printf("ack : %d\n", ntohl(header.ackNum));
			printf("Here, established change to fin_wait_1\n");
		}
	}
}

void TCPAssignment::syscall_read(UUID syscallUUID, int pid, int sd, void* buffer, int size)
{
	if(size <= 0)
	{
		returnSystemCall(syscallUUID, -1);
		return;
	}

	int result = -1;
	SocketDescriptor * temp = find_socket_descriptor(pid, sd);
	if(temp != NULL)
	{
		result = read_from_buffer(temp, buffer, size);
		if(result < 0)
		{
			temp->syscallUUID = syscallUUID;
			temp->input_buffer = buffer;
			temp->input_size = size;
			return; //Block Process
		}
	}

	returnSystemCall(syscallUUID, result);
	return;
}

int TCPAssignment::read_from_buffer(SocketDescriptor* sd, void* inputbuffer, int size)
{
	if(sd->read_buffer_head <=0)
		return -1;

	int read_size = (size > sd->read_buffer_head) ? sd->read_buffer_head : size;
 	memcpy(inputbuffer, sd->read_buffer, read_size);

 	if(read_size < sd->read_buffer_head)
 	{
 		memmove(sd->read_buffer, sd->read_buffer+read_size, sd->read_buffer_head - read_size); 
 	}

 	sd->read_buffer_head -= read_size;
 	//if(sd->read_buffer_head != 0)
 	//	printf("buffer head!! %d \n",sd->read_buffer_head);

 	return read_size;
}

void TCPAssignment::syscall_write(UUID syscallUUID, int pid, int sd, void* buffer, int size)
{
	SocketDescriptor * temp = find_socket_descriptor(pid, sd);

	if(temp != NULL)
	{
		if(size <= 0)
		{
			returnSystemCall(syscallUUID, -1);
		}
		else
		{
			if(temp->state == SocketState::ESTABLISHED || temp->state == SocketState::CLOSE_WAIT)
			{
				//if server's state is CLOSE_WAIT, it can still send data.
				if((temp->write_buffer_head + size) > DEFAULT_BUFFER_BYTE)
				{
					temp->input_buffer = buffer;
					temp->input_size = size;
					temp->syscallUUID = syscallUUID;
					return;
				}
				else
				{
					//else, copy buffer data to internal socket buffer.
					bool isStart = false;
					memcpy(temp->write_buffer + temp->write_buffer_head, buffer, size);
					if(temp->write_buffer_head == 0
						&& temp->write_packet_list->size() == 0
						&& temp->write_resend_list->size() == 0)
						isStart = true;

					temp->write_buffer_head += size;

					if(isStart)
					{
						write_from_buffer(temp);
					}

					returnSystemCall(syscallUUID, size);
					return;
				}
			}
		}
	}

	returnSystemCall(syscallUUID, -1);
	return;

}

void TCPAssignment::write_from_buffer(SocketDescriptor* sd)
{
	if(sd->syscallUUID > 0)
	{
		if(sd->input_buffer == NULL)
		{
			if(sd->write_packet_list->size() == 0 
				&& sd->write_resend_list->size() == 0
				&& sd->write_buffer_head == 0)
			{
				printf("byebye\n");
				send_close_packet(sd->syscallUUID, sd);
				return;
			}
		}
		else
		{
			if((sd->write_buffer_head + sd->input_size) <= DEFAULT_BUFFER_BYTE)
			{
				memcpy(sd->write_buffer + sd->write_buffer_head, sd->input_buffer, sd->input_size);
				sd->write_buffer_head += sd->input_size;
				returnSystemCall(sd->syscallUUID, sd->input_size);
				sd->input_buffer = NULL;
				sd->input_size = 0;
				sd->syscallUUID = -1;
			}
		}
	}
	
	while(sd->current_window_size > sd->write_packet_list->size())
	{
 		if(sd->write_buffer_head == 0 && sd->write_resend_list->size() == 0)
 			break;

 		WritePacket* newPacket = NULL;
 		if(sd->write_resend_list->size() > 0)
 		{
 			//printf("TIMEOUT RETTTTTTTTT\n");
 			newPacket = sd->write_resend_list->front();
 			sd->write_packet_list->push_back(newPacket);
 			sd->write_resend_list->pop_front();
 		}
 		else if(sd->write_buffer_head != 0)
 		{
			newPacket = new WritePacket();
			newPacket->size = (DEFAULT_MSS_BYTE > sd->write_buffer_head) ? sd->write_buffer_head : DEFAULT_MSS_BYTE;
			if(sd->write_packet_list->size() > 0)
				newPacket->seq = sd->write_packet_list->back()->expected_ack;
			else
				newPacket->seq = sd->seq;
			
			newPacket->expected_ack = newPacket->seq + newPacket->size;
			
			memcpy(newPacket->buffer, sd->write_buffer, newPacket->size);

			if(newPacket->size < sd->write_buffer_head)
	 		{
	 			memmove(sd->write_buffer, sd->write_buffer+newPacket->size, sd->write_buffer_head - newPacket->size); 
	 		}

 			sd->write_packet_list->push_back(newPacket);

	 		sd->write_buffer_head -= newPacket->size;
	 		newPacket->time = this->getHost()->getSystem()->getCurrentTime();
 		}

		TCPHeader header = {0};
		sockaddr_in* src_addr = (sockaddr_in*)sd->sock_addr;
		sockaddr_in* dest_addr = (sockaddr_in*)sd->peer_addr; 

		//printf("?? : %f\n", sd->timeout_interval);
		newPacket->rtt_timer = this->addTimer(newPacket, TimeUtil::makeTime(sd->timeout_interval * 1000 * 1000, TimeUtil::TimeUnit::NSEC));
		newPacket->sd = sd;	

		create_tcp_header(&header, src_addr, dest_addr, htonl(newPacket->seq), htonl(sd->current_expected_peer_seq), Flags::ACK, 1, newPacket->buffer, newPacket->size);
		Packet * packet = create_tcp_data_packet(sd->sock_addr, sd->peer_addr, &header, newPacket->buffer, newPacket->size);

		this->sendPacket("IPv4", packet);

		//printf("SNED !! expectiong %d\n", newPacket->expected_ack);
	}
 	
	/*
	//printf("YAY %d\n", write_packet_list->size());
	for(auto iter = sd->write_packet_list->begin(); iter != sd->write_packet_list->end(); ++iter)
	{
		WritePacket* p = *iter;

		//printf("YAY %d, %d", p->seq, p->size);
		
	}
	//printf("\n");
	*/
}

void TCPAssignment::syscall_connect(UUID syscallUUID, int pid, int s, sockaddr* addr, socklen_t len)
{
	//when we call connect(), then we immediately send sync packet. right?
	SocketDescriptor* temp = find_socket_descriptor(pid, s);
	if(temp != NULL)
	{
		sockaddr* newAddr = (sockaddr*)malloc(len);
		memcpy(newAddr, addr, len);
		temp->peer_addr = newAddr; 
		temp->peer_length = len;

		uint8_t src_ip[4];
		memcpy(src_ip, newAddr+sizeof(sa_family_t)+sizeof(uint16_t), 4);
 		//printf("Input IP %d.%d.%d.%d\n", src_ip[0], src_ip[1], src_ip[2], src_ip[3]);

		if(temp->sock_addr == NULL)
		{
			int newPort = this->getHost()->getRoutingTable(src_ip);
			this->getHost()->getIPAddr(src_ip, newPort);

			uint32_t int_addr;
			memcpy(&int_addr, src_ip, 4);

			sockaddr_in* newSock = (sockaddr_in*)malloc(sizeof(sockaddr_in));

			newSock->sin_family = AF_INET;
			newSock->sin_addr.s_addr = int_addr;
			newSock->sin_port = newPort;

			temp->sock_addr = (sockaddr*)newSock;
			temp->sock_length = sizeof(sockaddr);
		}
		else
		{
 			//printf("My New IP %d.%d.%d.%d\n", src_ip[0], src_ip[1], src_ip[2], src_ip[3]);	
			//printf("port %d\n", ntohs(newSock->sin_port));
		}

		temp->state = SocketState::SYN_SENT;

		TCPHeader syn_header = {0};
		sockaddr_in* src_addr = (sockaddr_in*)temp->sock_addr;
		sockaddr_in* dest_addr = (sockaddr_in*)addr; 
		create_tcp_header(&syn_header, src_addr, dest_addr, htonl(temp->seq), htonl(0), Flags::SYN, 1, NULL, 0);

		Packet * syn_packet = create_tcp_return_packet((sockaddr *)src_addr, (sockaddr *)dest_addr, &syn_header);
		this->sendPacket("IPv4", syn_packet);

		temp->seq += 1;
		temp->syscallUUID = syscallUUID;
		printf("UUID at syscall_connect: %lu\n", (uint64_t)syscallUUID);
	}
	else
	{
		returnSystemCall(syscallUUID, -1);
	}
}

void TCPAssignment::syscall_listen(UUID syscallUUID, int pid, int s, int backlog)
{
	printf("HEY LISTEN!\n");
	SocketDescriptor* temp = find_socket_descriptor(pid, s);
	if(temp != NULL)
	{
		if(temp->state != SocketState::CLOSED)
		{ 
			returnSystemCall(syscallUUID, -1);
			return;
		}
		else
		{
			temp->state = SocketState::LISTEN;
			temp->backlog = backlog;
			temp->backlog_queue = new std::list<ConnectionRequest*>;
			temp->connection_request_queue = new std::list<SocketDescriptor *>;
		}

		returnSystemCall(syscallUUID, 0);
		return;
	} 
	else
	{
		returnSystemCall( syscallUUID, -1);
		return;
	}
}

void TCPAssignment::syscall_accept(UUID syscallUUID, int pid, int s, sockaddr* addr, socklen_t* len)
{
	//returnSystemCall(syscallUUID,0);

	printf("SYSCALL ACCEPT==============================================%d %d\n",pid, s);
	SocketDescriptor* temp = find_socket_descriptor(pid, s);
	if(temp != NULL)
	{
		if(temp->state != SocketState::LISTEN)
		{
			returnSystemCall(syscallUUID, -1);
			return;
		}

		if(temp->connection_request_queue->size() > 0)
		{
			printf("Yay Accept Time!~!\n");

			auto newSd = temp->connection_request_queue->front();
			int newFd = createFileDescriptor(pid);
			if(newFd < 0 )
			{
				returnSystemCall(syscallUUID, -1);
				return;
			}
			newSd->fd = newFd;
			socketDescriptorMap->insert(SocketDescriptorPair(SocketDescriptorKey(newSd->pid, newSd->fd), newSd));
			temp->connection_request_queue->pop_front();

			memcpy(addr, newSd->peer_addr, sizeof(sockaddr));
			*len = sizeof(sockaddr);

			returnSystemCall(syscallUUID, newFd);
			return;
		}
		else
		{
			printf("Need to wait....\n");
			temp->syscallUUID = syscallUUID;
			temp->return_addr = addr;
			temp->return_addr_len = len;
			return;	
		}
		//we have to block until temp is ESTABLISHED.
	}
	else
	{
		returnSystemCall(syscallUUID, -1);
		return;
	}
}

void TCPAssignment::syscall_bind(UUID syscallUUID, int pid, int s, sockaddr* addr, socklen_t len)
{
	if(check_duplicate_port((sockaddr_in*)addr))
	{
		returnSystemCall(syscallUUID, -1);
		return;
	}

	SocketDescriptor* temp = find_socket_descriptor(pid, s);
	if(temp != NULL)
	{
		if(temp->sock_addr != NULL)
		{
			printf("second sd Number %d, %d\n",pid, s);
			returnSystemCall(syscallUUID,-1);
			return;
		}

		printf("first sd Number %d, %d\n",pid, s);
		sockaddr* newAddr = (sockaddr*)malloc(len);
		memcpy(newAddr, addr, len);
	
		temp->sock_addr = newAddr;
		temp->sock_length = len;

		returnSystemCall(syscallUUID, 0);
		return;
	}
	else
	{
		returnSystemCall(syscallUUID,-1);
		return;
	}

	
	returnSystemCall(syscallUUID,-1);
	return;
}

void TCPAssignment::syscall_getsockname(UUID syscallUUID, int pid, int s, sockaddr* addr, socklen_t* len)
{	
	SocketDescriptor* temp = find_socket_descriptor(pid, s);
	if(temp != NULL)
	{
		if(temp->sock_addr == NULL)
		{
			returnSystemCall(syscallUUID,-1);
			return;
		}

		*len = (socklen_t)sizeof(sockaddr);
		memcpy(addr, temp->sock_addr, sizeof(sockaddr));
		//memcpy(len, length, sizeof(socklen_t));

		returnSystemCall(syscallUUID,0);
		return;
	}

	returnSystemCall(syscallUUID,-1);
	return;
}

void TCPAssignment::syscall_getpeername(UUID syscallUUID, int pid, int s, sockaddr* addr, socklen_t* len)
{		
	//get the peer which is connected to given socket
	SocketDescriptor* temp = find_socket_descriptor(pid, s);
	if(temp != NULL)
	{
		if(temp->peer_addr == NULL)
		{
			returnSystemCall(syscallUUID,-1);
			return;
		}


		*len = (socklen_t)sizeof(sockaddr);
		memcpy(addr, temp->peer_addr, sizeof(sockaddr));

		returnSystemCall(syscallUUID,0);
		return;
	}

	returnSystemCall(syscallUUID,-1);
	return;
}

SocketDescriptor* TCPAssignment::allocate_socketDescriptor(int pid, int newFd, int type, int protocol)
{
	SocketDescriptor* newDescriptor = new SocketDescriptor();

	newDescriptor->pid = pid;
	newDescriptor->fd = newFd;
	newDescriptor->type = type;
	newDescriptor->protocol = protocol;
	newDescriptor->syscallUUID = -1;
	newDescriptor->state = SocketState::CLOSED;
	newDescriptor->backlog = 0;
	newDescriptor->sock_addr = NULL;
	newDescriptor->sock_length = 0;

	newDescriptor->peer_addr = NULL;
	newDescriptor->peer_length = 0;

	newDescriptor->seq = 0;

	newDescriptor->backlog_queue = NULL;
	newDescriptor->connection_request_queue = NULL;

	newDescriptor->return_addr = NULL;
	newDescriptor->return_addr_len = NULL;

	newDescriptor->resend_packet = NULL;
	newDescriptor->resend_time = 0;
	newDescriptor->timer = -1;

	newDescriptor->input_buffer = NULL;
	newDescriptor->input_size = 0;

	newDescriptor->write_buffer_head = 0;
	newDescriptor->write_buffer = (char *)malloc(sizeof(char) * DEFAULT_BUFFER_BYTE);
	newDescriptor->read_buffer_head = 0;
	newDescriptor->read_buffer = (char *)malloc(sizeof(char) * DEFAULT_BUFFER_BYTE);

	newDescriptor->write_packet_list = new std::list<WritePacket*>;
	newDescriptor->write_resend_list = new std::list<WritePacket*>;
	newDescriptor->receive_packet_list = new std::list<ReceivePacket*>;
	newDescriptor->current_window_size = 1;
	newDescriptor->current_ssh = 500;
	newDescriptor->current_success_count = 0;
	newDescriptor->current_fail_count = 0;
	newDescriptor->fail_ofs = 0;
	newDescriptor->timeout_ofs = 0;

	newDescriptor->sampleRTT = 100;
	newDescriptor->estimatedRTT = 100;
	newDescriptor->devRTT = 25;
	newDescriptor->timeout_interval = 200;

	newDescriptor->current_expected_peer_seq = 0;
	
	return newDescriptor;
}

void TCPAssignment::free_socketDescriptor(int pid, SocketDescriptor* sd)
{
	if(sd->sock_addr != NULL)
		free(sd->sock_addr);
	if(sd->peer_addr != NULL)
		free(sd->peer_addr);

	free(sd->write_buffer);
	free(sd->read_buffer);

	removeFileDescriptor(pid, sd->fd);
	delete(sd->write_packet_list);
	delete(sd->receive_packet_list);
	delete(sd);
}

bool TCPAssignment::check_duplicate_port(sockaddr_in* new_addr)
{
	bool result = false;
	for(auto iter = socketDescriptorMap->begin(); iter != socketDescriptorMap->end(); iter++)
	{
		SocketDescriptor* sd = iter->second;
		if(sd->sock_addr != NULL)
		{
			sockaddr_in* addr_in = (sockaddr_in*)sd->sock_addr;
			if(addr_in->sin_port == new_addr->sin_port &&
				(addr_in->sin_addr.s_addr == htonl(INADDR_ANY) ||
				 addr_in->sin_addr.s_addr == new_addr->sin_addr.s_addr))
			{
				result = true;
				break;
			} 
		}
	}
 
	return result;
}

SocketDescriptor* TCPAssignment::get_destination_port(sockaddr_in * target_addr)
{
	for(auto iter = socketDescriptorMap->begin(); iter != socketDescriptorMap->end(); iter++)
	{
		SocketDescriptor* sd = iter->second;
		if(sd->sock_addr != NULL)
		{
			sockaddr_in* addr_in = (sockaddr_in*)sd->sock_addr;
			if(addr_in->sin_port == target_addr->sin_port &&
				(addr_in->sin_addr.s_addr == htonl(INADDR_ANY) ||
				 addr_in->sin_addr.s_addr == target_addr->sin_addr.s_addr))
			{
				return sd;
			} 
		}
	}
 
 	return NULL;
}

SocketDescriptor* TCPAssignment::find_socket_descriptor(int pid, int sd)
{
	auto iter = socketDescriptorMap->find(SocketDescriptorKey(pid, sd));
	if(iter != socketDescriptorMap->end())
	{
		return iter->second;
	}
	return NULL;
}

bool TCPAssignment::get_flags(Flags type, TCPHeader * header)
{
	uint8_t flags = header->flags;
	int val = static_cast<int>(type);

	return get_bit(flags, val);
}

void TCPAssignment::set_flags(Flags type, int flag, TCPHeader * header)
{
	uint8_t flags = header->flags;
	int val = static_cast<int>(type);

	if(flag == 0)
	{
		header->flags = clear_bit(flags, val);
	}
	else if (flag == 1)
	{
		header->flags = set_bit(flags, val);
	}
}

bool TCPAssignment::get_bit(int number, int i)
{
	return ((number & (1 << i)) != 0);
}

int TCPAssignment::set_bit(int number, int i)
{
	return (number | (1 << i));
}

int TCPAssignment::clear_bit(int num, int i)
{
	int mask = ~(1 << i);
	return num & mask;
}

uint16_t TCPAssignment::get_tcp_checksum(uint32_t source_addr, uint32_t dest_addr, TCPHeader * header, void * data, int data_length)
{
	TCPHeader newHeader;
	memcpy(&newHeader, header, sizeof(TCPHeader));
 	newHeader.checksum = 0;

	char tcp_seg[sizeof(TCPHeader) + data_length];

 	memcpy(&tcp_seg, &newHeader, sizeof(TCPHeader));

 	if(data_length > 0)
 	{
 		memcpy(&tcp_seg[sizeof(TCPHeader)], data, data_length);	
 	}
 
	uint16_t checksum = NetworkUtil::tcp_sum(source_addr, 
                                             dest_addr, 
                                             (uint8_t *)&tcp_seg, 
                                             sizeof(TCPHeader) + data_length);
  checksum = ~checksum;
	checksum = htons(checksum);

	return checksum;
}

bool TCPAssignment::check_tcp_checksum(const TCPHeader* header, uint32_t src_ip_int, uint32_t dest_ip_int, void * data, int data_length)
{
	uint16_t header_checksum = header->checksum;
	TCPHeader newHeader;
	memcpy(&newHeader, header, sizeof(TCPHeader));
 	newHeader.checksum = 0;

 	char tcp_seg[sizeof(TCPHeader) + data_length];

 	memcpy(&tcp_seg, &newHeader, sizeof(TCPHeader));

 	if(data_length > 0)
 	{
 		memcpy(&tcp_seg[sizeof(TCPHeader)], data, data_length);	
 	}
 
	uint16_t checksum = NetworkUtil::tcp_sum(src_ip_int, 
										 	dest_ip_int, 
										 	(uint8_t *)&tcp_seg, 
										 	sizeof(TCPHeader) + data_length);
	checksum = ~checksum;
	checksum = htons(checksum);

	return (checksum == header_checksum);
}

void TCPAssignment::create_tcp_header(TCPHeader * header, sockaddr_in * src_addr, sockaddr_in * dest_addr, uint32_t seq, uint32_t ack, Flags flag, int set, void * data, int data_length)
{
	header->srcPort = src_addr->sin_port;
	header->destPort = dest_addr->sin_port;
	header->seqNum = seq;
	header->ackNum = ack;
	header->windowSize = htons(51200);
	header->offset_reserved_ns = 80;

	set_flags(flag, set, header);
	header->checksum = get_tcp_checksum(src_addr->sin_addr.s_addr, dest_addr->sin_addr.s_addr, header, data, data_length);

	return;
}

Packet * TCPAssignment::create_tcp_return_packet(sockaddr * src_addr, sockaddr* dest_addr, TCPHeader * header)
{
	uint32_t src_addr_int = ((sockaddr_in *)src_addr)->sin_addr.s_addr;
	uint32_t dest_addr_int = ((sockaddr_in *)dest_addr)->sin_addr.s_addr;

	Packet * syn_packet = this->allocatePacket(TCP_HEADER_SIZE);
	syn_packet->writeData(14+12, &src_addr_int, 4); 
	syn_packet->writeData(14+16, &dest_addr_int, 4);
	syn_packet->writeData(34, header, sizeof(TCPHeader));

	return syn_packet;
}

Packet * TCPAssignment::create_tcp_data_packet(sockaddr * src_addr, sockaddr* dest_addr, TCPHeader * header, void * data, int size)
{
	uint32_t src_addr_int = ((sockaddr_in *)src_addr)->sin_addr.s_addr;
	uint32_t dest_addr_int = ((sockaddr_in *)dest_addr)->sin_addr.s_addr;

	Packet * data_packet = this->allocatePacket(TCP_HEADER_SIZE + size);	

	data_packet->writeData(14+12, &src_addr_int, 4); 
	data_packet->writeData(14+16, &dest_addr_int, 4);
	data_packet->writeData(34, header, sizeof(TCPHeader));

	data_packet->writeData(34 + sizeof(TCPHeader), data, size);

	return data_packet;
}

int TCPAssignment::get_packet_data_length(Packet * packet)
{
	return (int) (packet->getSize() - (34 + sizeof(TCPHeader)));
}

double TCPAssignment::get_timeout_interval(SocketDescriptor * sd)
{
	get_estimated_rtt(sd);
	get_dev_rtt(sd);

	sd->timeout_interval = sd->estimatedRTT + 200.0f * sd->devRTT;
	//printf("return! %f\n",sd->timeout_interval);

	return sd->timeout_interval;
}

double TCPAssignment::get_estimated_rtt(SocketDescriptor * sd)
{
	sd->estimatedRTT = get_estimated_rtt(sd->sampleRTT, sd->estimatedRTT);
	return sd->estimatedRTT;
}

double TCPAssignment::get_estimated_rtt(double sampleRTT_cur, double estimatedRTT_before)
{
	double alpha = 0.125;

	double new_rtt = (1 - alpha) * estimatedRTT_before + alpha * sampleRTT_cur;

	//printf("before  : %f\n",estimatedRTT_before);
	//printf("current : %f\n",sampleRTT_cur);
	//printf("new_rtt : %f\n",new_rtt);
	return new_rtt;
}

double TCPAssignment::get_dev_rtt(SocketDescriptor * sd)
{
	sd->devRTT = get_dev_rtt(sd->sampleRTT, sd->estimatedRTT, sd->devRTT);
	return sd->devRTT;
}

double TCPAssignment::get_dev_rtt(double sampleRTT_cur, double estimatedRTT_cur, double devRTT_before)
{
	double beta = 0.25f;
	double new_dev_rtt = (1.0f - beta) * devRTT_before + beta * fabs((sampleRTT_cur - estimatedRTT_cur));
	//printf("dev_rtt : %f %f %f %f\n",new_dev_rtt, devRTT_before, sampleRTT_cur , estimatedRTT_cur);
	return 1;
}

}